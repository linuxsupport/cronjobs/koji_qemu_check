job "${PREFIX}_koji_qemu_check" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_koji_qemu_check" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/koji_qemu_check/koji_qemu_check:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_koji_qemu_check"
        }
      }
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      KOJICI_USER = "$KOJICI_USER"
      KOJICI_PWD = "$KOJICI_PWD"
      TAG = "${PREFIX}_koji_qemu_check"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 512 # MB
    }

  }
}

