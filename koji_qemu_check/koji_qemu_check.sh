#!/bin/bash
TIMEOUT=45

if [ -z $ADMIN_EMAIL ]; then
  ADMIN_EMAIL="lxsoft-admins@cern.ch"
fi

echo $KOJICI_PWD | kinit $KOJICI_USER &>/dev/null

function check_host {
  KERNEL=`ssh -o StrictHostKeyChecking=no -q root@$1 uname -r`
  UPTIME=`ssh -o StrictHostKeyChecking=no -q root@$1 uptime -p`
  CPU=`ssh -o StrictHostKeyChecking=no -q root@$1 'grep "model name" /proc/cpuinfo' | uniq | cut -d: -f2`
  CPUCOUNT=`ssh -o StrictHostKeyChecking=no -q root@$1 'grep "model name" /proc/cpuinfo | wc -l'`
  echo -n "Timing VM boot [4 VCPU/4096MB]] on $1 [$KERNEL kernel with $CPUCOUNT *$CPU CPU, $UPTIME]: "
  SECONDS=0
  timeout ${TIMEOUT}s ssh -o StrictHostKeyChecking=no -q root@$1 '/usr/libexec/qemu-kvm -m 4096 -smp 4 -enable-kvm -name perftest -nographic -append "rd.emergency=poweroff rd.shell=0" -cpu host -kernel `ls -tr1 /boot/vmlinuz*x86_64 |tail -n1` -initrd `ls -tr1 /boot/initramfs-*x86_64.img |tail -n1` &>/dev/null'
  if [ $? -eq 124 ]; then
    echo "TIMED OUT - waited $TIMEOUT seconds"
    # cleanup
    ssh -o StrictHostKeyChecking=no -q root@$1 'pkill -9 -f "qemu-kvm -m 4096 -smp 4 -enable-kvm -name perftest"'
    return 1
  else
    echo "COMPLETED - duration $SECONDS seconds"
    return 0
  fi
}

function do_admin_email {
  SUBJECT=$1
  BODY=$2
  MAILMX=cernmx.cern.ch
  FROM="Linux.Support@cern.ch"

  MAILTMP=`/bin/mktemp /tmp/$DIST$$.XXXXXXXX`
  echo "To: $ADMIN_EMAIL" >> $MAILTMP
  echo "From: $FROM" >> $MAILTMP
  echo "Reply-To: noreply.Linux.Support@cern.ch" >> $MAILTMP
  echo "Return-Path: $ADMIN_EMAIL" >> $MAILTMP
  echo "Subject: $SUBJECT" >> $MAILTMP

  echo -e "\nDear admins,\n" >> $MAILTMP
  # ensure shell globbing doesn't mess up our email
  set -f
  echo -e $BODY >> $MAILTMP
  set +f
  echo -e "\n---\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)" >> $MAILTMP

  cat $MAILTMP | swaks --server $MAILMX --to $ADMIN_EMAIL --from $FROM --helo cern.ch --data -
  rm -f $MAILTMP
}

TEST_HOSTS_BODY=`/bin/mktemp -u`
PROD_HOSTS_BODY=`/bin/mktemp -u`
EMAIL_BODY=`/bin/mktemp -u`
PROD_HOSTS=`koji list-hosts --arch x86_64 --enabled | grep koji | awk '{print $1}'`
TEST_HOSTS=`koji -p kojitest list-hosts --arch x86_64 --enabled | grep koji | awk '{print $1}'`
if [ ! -z "$PROD_HOSTS" ]; then
  for HOST in $PROD_HOSTS
  do
    check_host $HOST
    if [ $? -eq 1 ]; then
      echo "\n$HOST failed to boot a VM within $TIMEOUT seconds" >> $PROD_HOSTS_BODY
    fi
  done
fi
if [ ! -z "$TEST_HOSTS" ]; then
  for HOST in $TEST_HOSTS
  do
    check_host $HOST
    if [ $? -eq 1 ]; then
      echo "\n$HOST failed to boot a VM within $TIMEOUT seconds" >> $TEST_HOSTS_BODY
    fi
  done
fi
if [ -e $PROD_HOSTS_BODY ] || [ -e $TEST_HOSTS_BODY ]; then
  echo "\nToday we had at least 1 koji/kojitest host(s) that were not able to boot a simple kernel via qemu.\nYou may want to investigate/reboot the following host(s):\n" >> $EMAIL_BODY
  cat $PROD_HOSTS_BODY >> $EMAIL_BODY
  cat $TEST_HOSTS_BODY >> $EMAIL_BODY
fi
if [ -e $EMAIL_BODY ]; then
  do_admin_email "Koji qemu performance issue" "`cat $EMAIL_BODY`"  
fi
